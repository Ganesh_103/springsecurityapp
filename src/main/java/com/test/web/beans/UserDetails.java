package com.test.web.beans;

public class UserDetails {
	private String userName;
	private String userId;
	private String userLocation;
	private String userQualification;

	public UserDetails(String userName, String userId, String userLocation, String userQualification) {
		this.userName = userName;
		this.userId = userId;
		this.userLocation = userLocation;
		this.userQualification = userQualification;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserLocation() {
		return userLocation;
	}

	public void setUserLocation(String userLocation) {
		this.userLocation = userLocation;
	}

	public String getUserQualification() {
		return userQualification;
	}

	public void setUserQualification(String userQualification) {
		this.userQualification = userQualification;
	}
}
