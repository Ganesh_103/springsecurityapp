package com.test.web.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class TestController {

	@RequestMapping(value = "/name", method = {RequestMethod.GET,RequestMethod.POST})
	public String getName() {
		String s = "{\r\n" + 
				"	\"fulfillmentText\": \"Ganesh Peram\"\r\n" + 
				"}";
		return s;
	}
	
	@RequestMapping(value = "/village", method = {RequestMethod.GET,RequestMethod.POST})
	public String getVillage() {
		String s = "{\r\n" + 
				"	\"fulfillmentText\": \"Banglore\"\r\n" + 
				"}";
		return s;
	}
	
	
	
	@GetMapping("/hello")
	public ModelAndView getIndexPage1() {
		System.out.println("getting the appl");
		return new ModelAndView("hello");
	}
}
