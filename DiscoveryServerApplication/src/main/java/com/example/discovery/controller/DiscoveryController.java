package com.example.discovery.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class DiscoveryController {

	{
		System.out.println("#######  DiscoveryController");
	}
	
	@GetMapping("/getUserData")
	@ResponseBody
	public String getUserInfo() {
		return "<h1><font color="+"red"+">Ganesh Peram</font></h1>";
	}
	
	@GetMapping("/getQualification")
	public String getQual() {
		return "B.Tech";
	}
}
